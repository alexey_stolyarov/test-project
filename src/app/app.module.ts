import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import {notesListReducer} from "./core/reducers/notes-list.reducer";
import {SharedModule} from "./shared/shared.module";
import {noteViewStateReducer} from "./core/reducers/note-view.reducer";
import {noteEditStateReducer} from "./core/reducers/note-edit.reducer";
import {noteAddStateReducer} from "./core/reducers/note-add.reducer";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    StoreModule.forRoot({
      notesListState: notesListReducer,
      noteViewState: noteViewStateReducer,
      noteEditState: noteEditStateReducer,
      noteAddState: noteAddStateReducer
    }, {})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
