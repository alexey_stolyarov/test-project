import { Injectable } from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {NotesListStateModel} from "../../models/states/notes-list-state.model";
import {NotesListSelectors} from "../../selectors/notes-list.selectors";
import {find, map, mergeAll} from "rxjs/operators";
import {NoteModel} from "../../models/note.model";
import {NotesListActions} from "../../actions/notes-list.actions";

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  private notesListState$: Observable<NotesListStateModel>;

  constructor(private store: Store) {
    this.notesListState$ = this.store.select(NotesListSelectors.selectNotesListState);
  }

  getNotes(): Observable<NoteModel[]> {
    return this.notesListState$.pipe(
      map((notesListState) => notesListState.notesList)
    );
  }

  getNote(id: number) {
    return this.notesListState$.pipe(
      map(notesListState => notesListState.notesList),
      mergeAll(),
      find((note: NoteModel) => note.id == id)
    )
  }

  deleteNote(note: NoteModel) {
    this.store.dispatch(NotesListActions.deleteNote(note));
  }

  addNote(note: NoteModel) {
    this.store.dispatch(NotesListActions.pushNote(note));
  }
}
