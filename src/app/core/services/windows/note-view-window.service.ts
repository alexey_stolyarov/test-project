import { Injectable } from '@angular/core';
import {Store} from "@ngrx/store";
import {NoteViewActions} from "../../actions/note-view.actions";
import {NoteModel} from "../../models/note.model";
import {Observable} from "rxjs";
import {NoteViewSelectors} from "../../selectors/note-view.selectors";
import {NotesService} from "../notes/notes.service";
import {first} from "rxjs/operators";
import {NotableWindowService} from "./abstracts/notable-window.service";

@Injectable({
  providedIn: 'root'
})
export class NoteViewWindowService implements NotableWindowService {
  constructor(private store: Store,
              private notesService: NotesService) {
  }
  showWindow(id: number) {
    const sub = this.notesService.getNote(id).pipe(first()).subscribe(note => {
      this.store.dispatch(NoteViewActions.showNote({
        isShown: true,
        note: note
      }))
    });
    sub.unsubscribe();
  }
  closeWindow() {
    this.store.dispatch(NoteViewActions.closeNote());
  }

  getNote(): Observable<NoteModel> {
    return this.store.select(NoteViewSelectors.selectNote);
  }
}
