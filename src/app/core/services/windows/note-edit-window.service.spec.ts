import { TestBed } from '@angular/core/testing';

import { NoteEditWindowService } from './note-edit-window.service';

describe('NoteEditWindowService', () => {
  let service: NoteEditWindowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoteEditWindowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
