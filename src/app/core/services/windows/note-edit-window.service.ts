import { Injectable } from '@angular/core';
import {NotableWindowService} from "./abstracts/notable-window.service";
import {Store} from "@ngrx/store";
import {NotesService} from "../notes/notes.service";
import {first} from "rxjs/operators";
import {Observable} from "rxjs";
import {NoteModel} from "../../models/note.model";
import {NoteEditActions} from "../../actions/note-edit.actions";
import {NoteEditSelectors} from "../../selectors/note-edit.selectors";
import {NotesListActions} from "../../actions/notes-list.actions";

@Injectable({
  providedIn: 'root'
})
export class NoteEditWindowService implements NotableWindowService {

  constructor(private store: Store,
              private notesService: NotesService) {
  }
  showWindow(id: number) {
    const sub = this.notesService.getNote(id).pipe(first()).subscribe(note => {
      this.store.dispatch(NoteEditActions.showNote({
        isShown: true,
        note: note
      }));
    });
    sub.unsubscribe();
  }
  closeWindow() {
    this.store.dispatch(NoteEditActions.closeNote());
  }

  getNote(): Observable<NoteModel> {
    return this.store.select(NoteEditSelectors.selectNote);
  }

  editNode(note: NoteModel) {
    this.store.dispatch(NotesListActions.editNote(note));
  }
}
