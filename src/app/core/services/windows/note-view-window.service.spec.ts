import { TestBed } from '@angular/core/testing';

import { NoteViewWindowService } from './note-view-window.service';

describe('NoteViewWindowService', () => {
  let service: NoteViewWindowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoteViewWindowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
