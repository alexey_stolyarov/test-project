import {WindowsService} from "./windows.service";
import {Observable} from "rxjs";
import {NoteModel} from "../../../models/note.model";

export abstract class NotableWindowService extends WindowsService {
  abstract getNote(): Observable<NoteModel>;
}
