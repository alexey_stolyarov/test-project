export abstract class WindowsService {
  abstract showWindow(id: number)
  abstract closeWindow()
}
