import { Injectable } from '@angular/core';
import {WindowsService} from "./abstracts/windows.service";
import {Store} from "@ngrx/store";
import {NotesService} from "../notes/notes.service";
import {NoteViewActions} from "../../actions/note-view.actions";
import {NoteAddActions} from "../../actions/note-add.actions";
import {NoteModel} from "../../models/note.model";

@Injectable({
  providedIn: 'root'
})
export class NoteAddWindowService implements WindowsService {
  constructor(private notesService: NotesService,
              private store: Store) {

  }
  showWindow(id: number) {
    this.store.dispatch(NoteAddActions.showNote());
  }
  closeWindow() {
    this.store.dispatch(NoteViewActions.closeNote());
  }
  addNote(note: NoteModel) {
    this.notesService.addNote(note);
  }
}
