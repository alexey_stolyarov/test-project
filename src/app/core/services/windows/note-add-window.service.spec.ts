import { TestBed } from '@angular/core/testing';

import { NoteAddWindowService } from './note-add-window.service';

describe('NoteAddWindowService', () => {
  let service: NoteAddWindowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoteAddWindowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
