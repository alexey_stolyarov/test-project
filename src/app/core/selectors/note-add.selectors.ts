import {createFeatureSelector, createSelector} from "@ngrx/store";
import {WindowStateModel} from "../models/states/window-state.model";

export namespace NoteAddSelectors {
  export const selectNoteAddState = createFeatureSelector<WindowStateModel>('noteAddState');
  export const selectNoteShown = createSelector(selectNoteAddState, (state) => state.isShown);
}
