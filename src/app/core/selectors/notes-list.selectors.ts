import {createFeatureSelector, createSelector} from "@ngrx/store";
import {NotesListStateModel} from "../models/states/notes-list-state.model";

export namespace NotesListSelectors {
  export const selectNotesListState = createFeatureSelector<NotesListStateModel>('notesListState');

  export const selectNotesListTitlesState =
    createSelector(
      selectNotesListState,
      (state) => {
        state.notesList.map((note) => {
          note.title;
        });
      }
    )
}
