import {createFeatureSelector, createSelector} from "@ngrx/store";
import {NotableWindowStateModel} from "../models/states/notable-window-state.model";

export namespace NoteEditSelectors {
  export const selectNoteEditState = createFeatureSelector<NotableWindowStateModel>('noteEditState');
  export const selectNote = createSelector(selectNoteEditState, (state) => state.note);
  export const selectNoteShown = createSelector(selectNoteEditState, (state) => state.isShown);
}
