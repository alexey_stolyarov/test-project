import {createFeatureSelector, createSelector} from "@ngrx/store";
import {NotableWindowStateModel} from "../models/states/notable-window-state.model";

export namespace NoteViewSelectors {
  export const selectNoteViewState = createFeatureSelector<NotableWindowStateModel>('noteViewState');
  export const selectNote = createSelector(selectNoteViewState, (state) => state.note);
  export const selectNoteShown = createSelector(selectNoteViewState, (state) => state.isShown);
}
