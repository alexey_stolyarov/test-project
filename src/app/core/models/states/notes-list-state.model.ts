import {NoteModel} from "../note.model";

export interface NotesListStateModel {
  notesList: NoteModel[],
}
