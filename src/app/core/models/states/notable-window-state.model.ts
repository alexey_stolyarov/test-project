import {WindowStateModel} from "./window-state.model";
import {NoteModel} from "../note.model";

export interface NotableWindowStateModel extends WindowStateModel {
  note: NoteModel | null;
}
