import {createAction, props} from '@ngrx/store';
import {NotableWindowStateModel} from "../models/states/notable-window-state.model";

export namespace NoteViewActions {
  export const showNote = createAction('[Note Card] Show Card', props<NotableWindowStateModel>());
  export const closeNote = createAction('[Note Card] Close Card');
}
