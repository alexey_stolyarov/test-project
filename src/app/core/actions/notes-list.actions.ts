import {createAction, props} from "@ngrx/store";
import {NotesListStateModel} from "../models/states/notes-list-state.model";
import {NoteModel} from "../models/note.model";

export namespace NotesListActions {
  export const pushNotes = createAction(
    '[Notes List Component] Get Notes List',
    props<NotesListStateModel>()
  );

  export const pushNote = createAction(
    '[Main Component] Push New Note',
    props<NoteModel>());

  export const editNote = createAction(
    '[Main Component] Edit Note',
    props<NoteModel>());

  export const deleteNote = createAction(
    '[Main Component] Delete Note',
    props<NoteModel>());
}
