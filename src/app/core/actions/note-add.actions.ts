import {createAction} from '@ngrx/store';

export namespace NoteAddActions {
  export const showNote = createAction('[Note Add] Show Note Add');
  export const closeNote = createAction('[Note Add] Close Note Add');
}
