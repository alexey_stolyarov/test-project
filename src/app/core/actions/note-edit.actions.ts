import {createAction, props} from '@ngrx/store';
import {NotableWindowStateModel} from "../models/states/notable-window-state.model";

export namespace NoteEditActions {
  export const showNote = createAction('[Note Edit] Show Note Edit', props<NotableWindowStateModel>());
  export const closeNote = createAction('[Note Edit] Close Note Edit');
}
