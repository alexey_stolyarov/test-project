import {createReducer, on} from "@ngrx/store";
import {WindowStateModel} from "../models/states/window-state.model";
import {NoteAddActions} from "../actions/note-add.actions";

const initialNoteAddState: WindowStateModel = {
  isShown: false
};

export const noteAddStateReducer = createReducer(
  initialNoteAddState,
  on(
    NoteAddActions.showNote, (_noteAddState: WindowStateModel): WindowStateModel => ({
      ..._noteAddState,
      isShown: true
    })
  ),
  on(
    NoteAddActions.closeNote, (_noteAddState: WindowStateModel): WindowStateModel => ({
      ..._noteAddState,
      isShown: false
    })
  )
)
