import {NotesListStateModel} from "../models/states/notes-list-state.model";
import {createReducer, on} from "@ngrx/store";
import {NotesListActions} from "../actions/notes-list.actions";
import {NoteModel} from "../models/note.model";

const initialNotesListState: NotesListStateModel = {
  notesList: [
    {
      id: 0,
      title: 'A',
      description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,\n' +
        'molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum\n' +
        'numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium\n' +
        'optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis\n' +
        'obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam\n' +
        'nihil, eveniet aliquid culpa officia aut! Impedit sit sunt quaerat, odit,\n' +
        'tenetur error, harum nesciunt ipsum debitis quas aliquid. Reprehenderit,\n' +
        'quia. Quo neque error repudiandae fuga? Ipsa laudantium molestias eos \n' +
        'sapiente officiis modi at sunt excepturi expedita sint? Sed quibusdam\n' +
        'recusandae alias error harum maxime adipisci amet laborum. Perspiciatis \n' +
        'minima nesciunt dolorem! Officiis iure rerum voluptates a cumque velit \n' +
        'quibusdam sed amet tempora. Sit laborum ab, eius fugit doloribus tenetur \n' +
        'fugiat, temporibus enim commodi iusto libero magni deleniti quod quam \n' +
        'consequuntur! Commodi minima excepturi repudiandae velit hic maxime\n' +
        'doloremque. Quaerat provident commodi consectetur veniam similique ad \n' +
        'earum omnis ipsum saepe, voluptas, hic voluptates pariatur est explicabo \n' +
        'fugiat, dolorum eligendi quam cupiditate excepturi mollitia maiores labore \n' +
        'suscipit quas? Nulla, placeat. Voluptatem quaerat non architecto ab laudantium\n' +
        'modi minima sunt esse temporibus sint culpa, recusandae aliquam numquam \n' +
        'totam ratione voluptas quod exercitationem fuga. Possimus quis earum veniam \n' +
        'quasi aliquam eligendi, placeat qui corporis!'
    },
    {
      id: 1,
      title: 'B',
      description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,\n' +
        'molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum\n' +
        'numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium\n' +
        'optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis\n' +
        'obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam\n' +
        'nihil, eveniet aliquid culpa officia aut! Impedit sit sunt quaerat, odit,\n' +
        'tenetur error, harum nesciunt ipsum debitis quas aliquid. Reprehenderit,\n' +
        'quia. Quo neque error repudiandae fuga? Ipsa laudantium molestias eos \n' +
        'sapiente officiis modi at sunt excepturi expedita sint? Sed quibusdam\n' +
        'recusandae alias error harum maxime adipisci amet laborum. Perspiciatis \n' +
        'minima nesciunt dolorem! Officiis iure rerum voluptates a cumque velit \n' +
        'quibusdam sed amet tempora. Sit laborum ab, eius fugit doloribus tenetur \n' +
        'fugiat, temporibus enim commodi iusto libero magni deleniti quod quam \n' +
        'consequuntur! Commodi minima excepturi repudiandae velit hic maxime\n' +
        'doloremque. Quaerat provident commodi consectetur veniam similique ad \n' +
        'earum omnis ipsum saepe, voluptas, hic voluptates pariatur est explicabo \n' +
        'fugiat, dolorum eligendi quam cupiditate excepturi mollitia maiores labore \n' +
        'suscipit quas? Nulla, placeat. Voluptatem quaerat non architecto ab laudantium\n' +
        'modi minima sunt esse temporibus sint culpa, recusandae aliquam numquam \n' +
        'totam ratione voluptas quod exercitationem fuga. Possimus quis earum veniam \n' +
        'quasi aliquam eligendi, placeat qui corporis!'
    },
    {
      id: 2,
      title: 'C',
      description: 'C'
    }
  ]
};

export const notesListReducer = createReducer(
  initialNotesListState,
  on(
    NotesListActions.pushNotes, (_notes: NotesListStateModel, notes: NotesListStateModel): NotesListStateModel => ({
      ..._notes,
      notesList: notes.notesList
    })
  ),
  on(
    NotesListActions.pushNote, (_notes: NotesListStateModel, note: NoteModel): NotesListStateModel => ({
      ..._notes,
      notesList: [..._notes.notesList, note]
    })
  ),
  on(
    NotesListActions.editNote, (_notes: NotesListStateModel, note: NoteModel): NotesListStateModel => ({
      ..._notes,
      notesList: _notes.notesList.map((currentNote) => currentNote.id == note.id ? note : currentNote)
    })
  ),
  on(
    NotesListActions.deleteNote, (_notes: NotesListStateModel, note: NoteModel): NotesListStateModel => ({
      ..._notes,
      notesList: _notes.notesList.filter((currentNote) => currentNote.id != note.id)
    })
  )
)
