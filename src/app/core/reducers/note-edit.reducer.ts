import {createReducer, on} from "@ngrx/store";
import {NotableWindowStateModel} from "../models/states/notable-window-state.model";
import {NoteEditActions} from "../actions/note-edit.actions";

const initialNoteEditState: NotableWindowStateModel = {
  isShown: false,
  note: null
};

export const noteEditStateReducer = createReducer(
  initialNoteEditState,
  on(
    NoteEditActions.showNote, (_noteEditState: NotableWindowStateModel, noteEditState: NotableWindowStateModel): NotableWindowStateModel => ({
      ..._noteEditState,
      isShown: true,
      note: {
        id: noteEditState.note.id,
        title: noteEditState.note.title,
        description: noteEditState.note.description
      }
    })
  ),
  on(
    NoteEditActions.closeNote, (_noteEditState: NotableWindowStateModel): NotableWindowStateModel => ({
      ..._noteEditState,
      isShown: false,
      note: null
    })
  )
)
