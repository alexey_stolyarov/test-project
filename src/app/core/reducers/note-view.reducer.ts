import {createReducer, on} from "@ngrx/store";
import {NoteViewActions} from "../actions/note-view.actions";
import {NotableWindowStateModel} from "../models/states/notable-window-state.model";

const initialNoteViewState: NotableWindowStateModel = {
  isShown: false,
  note: null
};

export const noteViewStateReducer = createReducer(
  initialNoteViewState,
  on(
    NoteViewActions.showNote, (_noteViewState: NotableWindowStateModel, noteViewState: NotableWindowStateModel): NotableWindowStateModel => ({
      ..._noteViewState,
      isShown: true,
      note: {
        id: noteViewState.note.id,
        title: noteViewState.note.title,
        description: noteViewState.note.description
      }
    })
  ),
  on(
    NoteViewActions.closeNote, (_noteViewState: NotableWindowStateModel): NotableWindowStateModel => ({
      ..._noteViewState,
      isShown: false,
      note: null
    })
  )
)
