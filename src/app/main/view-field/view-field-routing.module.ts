import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ViewFieldComponent} from "./view-field.component";

const routes: Routes = [
  { path: '', component: ViewFieldComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewFieldRoutingModule { }
