import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewFieldComponent } from './view-field.component';
import { ViewFieldRoutingModule } from "./view-field-routing.module";
import {MatCardModule} from "@angular/material/card";



@NgModule({
  declarations: [ViewFieldComponent],
  exports: [
    ViewFieldComponent
  ],
    imports: [
        CommonModule,
        ViewFieldRoutingModule,
        MatCardModule
    ]
})
export class ViewFieldModule { }
