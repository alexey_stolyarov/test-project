import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddNoteRoutingModule } from './add-note-routing.module';
import { AddNoteComponent } from './add-note.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";


@NgModule({
  declarations: [AddNoteComponent],
  imports: [
    CommonModule,
    AddNoteRoutingModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ]
})
export class AddNoteModule { }
