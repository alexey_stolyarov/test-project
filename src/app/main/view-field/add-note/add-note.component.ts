import { Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {WindowComponent} from "../../../shared/components/window.component";
import {NotesService} from "../../../core/services/notes/notes.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NoteAddWindowService} from "../../../core/services/windows/note-add-window.service";
import {NoteModel} from "../../../core/models/note.model";
import {map} from "rxjs/operators";
import {Subscription} from "rxjs";

@Component({
  selector: 'tp-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent extends WindowComponent{

  addFormGroup: FormGroup;

  notesIdsSub: Subscription;
  maxId: number = 0;

  constructor(protected noteAddWindowService: NoteAddWindowService,
              protected notesService: NotesService,
              protected activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router) {
    super(noteAddWindowService, notesService, activatedRoute);
  }


  ngOnInit() {
    super.ngOnInit();
    this.notesIdsSub = this.notesService.getNotes().pipe(
      map((notes) => notes.map(notes => notes.id))
    ).subscribe((notesIds) => {
      if (notesIds.length) {
        this.maxId = Math.max(...notesIds);
      }
    });
    this.addFormGroup = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.notesIdsSub.unsubscribe();
    this.noteAddWindowService.closeWindow();
  }

  onSubmit() {
    const note: NoteModel = {
      id: (this.maxId + 1),
      title: this.addFormGroup.get('title').value,
      description: this.addFormGroup.get('content').value
    };
    this.router.navigate(['notes', note.id]).then(() => {
      this.noteAddWindowService.addNote(note);
    });
  }

}
