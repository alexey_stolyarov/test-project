import { Component } from '@angular/core';
import {NotesService} from "../../../core/services/notes/notes.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NotableWindowComponent} from "../../../shared/components/notable-window.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NoteEditWindowService} from "../../../core/services/windows/note-edit-window.service";
import {NoteModel} from "../../../core/models/note.model";
@Component({
  selector: 'tp-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss']
})
export class EditNoteComponent extends NotableWindowComponent {

  editFormGroup: FormGroup;

  constructor(protected notesService: NotesService,
              protected activatedRoute: ActivatedRoute,
              protected noteEditService: NoteEditWindowService,
              private formBuilder: FormBuilder,
              private router: Router) {
    super(noteEditService, notesService, activatedRoute);
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.note) {
      this.editFormGroup = this.formBuilder.group({
        title: [this.note.title, Validators.required],
        content: [this.note.description, Validators.required]
      });
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.noteEditService.closeWindow();
  }

  onSubmit() {
    const note: NoteModel = {
      id: this.note.id,
      title: this.editFormGroup.get('title').value,
      description: this.editFormGroup.get('content').value
    };
    this.router.navigate(['notes', this.note.id]).then(() => {
      this.noteEditService.editNode(note);
    });
  }
}
