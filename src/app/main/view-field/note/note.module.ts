import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteComponent } from './note.component';
import {NoteRoutingModule} from "./note-routing.module";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [NoteComponent],
    imports: [
        CommonModule,
        NoteRoutingModule,
        MatCardModule,
        MatButtonModule
    ]
})
export class NoteModule { }
