import {Component} from '@angular/core';
import {NoteViewWindowService} from "../../../core/services/windows/note-view-window.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NotesService} from "../../../core/services/notes/notes.service";
import {NotableWindowComponent} from "../../../shared/components/notable-window.component"
import {NoteEditWindowService} from "../../../core/services/windows/note-edit-window.service";

@Component({
  selector: 'tp-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent extends NotableWindowComponent {

  constructor(protected notesService: NotesService,
              protected activatedRoute: ActivatedRoute,
              protected noteViewService: NoteViewWindowService,
              private noteViewWindowService: NoteViewWindowService,
              private noteEditWindowService: NoteEditWindowService,
              private router: Router) {
    super(noteViewService, notesService, activatedRoute);
  }

  deleteNote(): void {
    this.router.navigate(['/notes']).then(() => {
      this.notesService.deleteNote(this.note);
    })
  }

  editNote(): void {
    this.router.navigate(['notes', this.note.id, 'edit']).then(() => {
      this.noteEditWindowService.showWindow(this.note.id);
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.noteViewWindowService.closeWindow();
  }

}
