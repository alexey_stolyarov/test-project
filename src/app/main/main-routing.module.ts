import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main.component';

const routes: Routes = [
  { path: '', component: MainComponent, children: [
      {path: 'add', loadChildren: () => import('./view-field/add-note/add-note.module').then(m => m.AddNoteModule), pathMatch: 'full' },
      {path: ':id', loadChildren: () => import('./view-field/note/note.module').then(m => m.NoteModule)},
      {path: ':id/edit', loadChildren: () => import('./view-field/edit-note/edit-note.module').then(m => m.EditNoteModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
