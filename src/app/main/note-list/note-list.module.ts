import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NoteListComponent} from "./note-list.component";
import {MatListModule} from "@angular/material/list";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [NoteListComponent],
  exports: [
    NoteListComponent
  ],
  imports: [
    CommonModule,
    MatListModule,
    RouterModule
  ]
})
export class NoteListModule { }
