import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotesService} from "../../core/services/notes/notes.service";
import {Observable, Subscription} from "rxjs";
import {NoteModel} from "../../core/models/note.model";
import {Router} from "@angular/router";
import {NoteViewWindowService} from "../../core/services/windows/note-view-window.service";

@Component({
  selector: 'tp-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnInit, OnDestroy {

  notesList$: Observable<NoteModel[]>
  notesListSub: Subscription

  constructor(private notesService: NotesService,
              private router: Router,
              private noteViewWindowService: NoteViewWindowService) {
  }

  ngOnInit(): void {
    this.notesList$ = this.notesService.getNotes();
    this.notesListSub = this.notesList$.subscribe();
  }

  ngOnDestroy(): void {
    this.notesListSub.unsubscribe();
  }

  onNoteClicked(note: NoteModel): void {
    this.router.navigate(['notes', note.id]).then(() => {
      this.noteViewWindowService.showWindow(note.id)
    });
  }

}
