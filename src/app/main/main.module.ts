import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import {ViewFieldModule} from "./view-field/view-field.module";
import {NoteListModule} from "./note-list/note-list.module";


@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    ViewFieldModule,
    NoteListModule
  ]
})
export class MainModule { }
