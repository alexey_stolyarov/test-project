import {WindowComponent} from "./window.component";
import {NotesService} from "../../core/services/notes/notes.service";
import {ActivatedRoute} from "@angular/router";
import {Observable, Subscription} from "rxjs";
import {NoteModel} from "../../core/models/note.model";
import {OnDestroy, OnInit} from "@angular/core";
import {NotableWindowService} from "../../core/services/windows/abstracts/notable-window.service";

export abstract class NotableWindowComponent extends WindowComponent implements OnInit, OnDestroy{

  note$: Observable<NoteModel>;
  noteSub: Subscription;
  note: NoteModel;

  protected constructor(protected notableWindowService: NotableWindowService,
                      protected notesService: NotesService,
                      protected activatedRoute: ActivatedRoute) {
    super(notableWindowService, notesService, activatedRoute);
  }
  ngOnInit() {
    super.ngOnInit();
    this.note$ = this.notableWindowService.getNote();
    this.noteSub = this.note$.subscribe((note) => {
      this.note = note;
    });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.noteSub.unsubscribe();
  }


}
