import {OnDestroy, OnInit} from "@angular/core";
import {Observable, Subscription} from "rxjs";
import {ActivatedRoute, UrlSegment} from "@angular/router";
import {NotesService} from "../../core/services/notes/notes.service";
import {map} from "rxjs/operators";
import {WindowsService} from "../../core/services/windows/abstracts/windows.service";

export abstract class WindowComponent implements OnInit, OnDestroy {

  activatedRoute$: Observable<UrlSegment[]>;
  activatedRouteSub: Subscription;

  protected constructor(protected windowsService: WindowsService,
                        protected notesService: NotesService,
                        protected activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute$ = this.activatedRoute.parent.url;
    this.activatedRouteSub = this.activatedRoute$.pipe(
      map((urlSegment) => urlSegment[0].path)
    ).subscribe((url) => {
      this.windowsService.showWindow(Number(url));
    });
  }

  ngOnDestroy(): void {
    this.activatedRouteSub.unsubscribe();
  }
}
